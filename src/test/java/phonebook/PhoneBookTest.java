package phonebook;

import org.junit.Test;

import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static phonebook.PhoneBook.getPhoneBook;

public class PhoneBookTest {

    @Test
    public void getNumber() {
        Map<String, List<String>> phoneBook = getPhoneBook();
        List<String> list = phoneBook.get("Петров П.П.");
        assertEquals(list.toString(), "[+8 800 2000 700]");
    }

    @Test
    public void falseName() {
        Map<String, List<String>> phoneBook = getPhoneBook();
        List<String> list = phoneBook.get("Петрович");
        assertNull(list);
    }
}