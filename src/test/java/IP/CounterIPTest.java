package IP;

import org.junit.Test;

import java.net.UnknownHostException;

import static org.junit.Assert.*;

public class CounterIPTest {

    @Test
    public void getCounterIP() throws UnknownHostException {
            CounterIP counterIP = new CounterIP();
            String text = counterIP.getIpRange(CounterIP.ipToLong("192.168.0.235"), counterIP.ipToLong("192.168.0.238"));
            assertEquals(text, "192.168.0.236\n" + "192.168.0.237\n");
    }

    @Test
    public  void  getCounterIPFail() throws UnknownHostException {
        CounterIP counterIP = new CounterIP();
        String text = counterIP.getIpRange(CounterIP.ipToLong("192.168.33.55"), counterIP.ipToLong("192.168.32.254"));
        assertEquals(text, "Неверный диапазон адресов!");
    }
}