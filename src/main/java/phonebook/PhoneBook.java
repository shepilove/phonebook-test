package phonebook;

import java.util.*;

public class PhoneBook {
    public static void main(String[] args) {
        Map<String, List<String>> phoneBook = getPhoneBook();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите фамилию и инициалы: ");
        String name = scanner.nextLine();
        List<String> list = phoneBook.get(name);

        if (list == null) {
            System.out.println("Такого ФИО в БД нет");
        } else {
            for (String number : list){
                System.out.printf("%s. %s \n", (list.indexOf(number) + 1), number);
            }
        }
    }

    protected static Map<String, List<String>> getPhoneBook() {
        Map<String, List<String>> phoneBook = new HashMap<String, List<String>>();

        List<String> number1 = new ArrayList<String>();
        number1.add("+8 800 2000 500");
        number1.add("+8 800 2000 600");

        List<String> number2 = new ArrayList<String>();
        number2.add("+8 800 2000 700");

        List<String> number3 = new ArrayList<String>();
        number3.add("+8 800 2000 800");
        number3.add("+8 800 2000 900 ");
        number3.add("+8 800 2000 000");

        phoneBook.put("Иванов И.И.", number1);
        phoneBook.put("Петров П.П.", number2);
        phoneBook.put("Сидоров С.С.", number3);

        return phoneBook;
    }
}
