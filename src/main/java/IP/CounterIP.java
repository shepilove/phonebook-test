package IP;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.util.Scanner;

public class CounterIP {
    public static void main(String[] args) throws UnknownHostException {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите начальный IP адрес: ");
        String ipStart = scanner.nextLine();
        System.out.println("Введите конечный IP адрес: ");
        String ipEnd = scanner.nextLine();

        Long numOfIpStart = ipToLong(ipStart);
        Long numOfIpEnd = ipToLong(ipEnd);

        String ips = getIpRange(numOfIpStart, numOfIpEnd);
        System.out.println(ips);
    }

    protected static String getIpRange(Long numOfIpStart, Long numOfIpEnd) throws UnknownHostException {
        if (numOfIpEnd < numOfIpStart){
            return "Неверный диапазон адресов!";
        }
        StringBuilder builder = new StringBuilder();
        while (numOfIpStart < numOfIpEnd - 1){
            numOfIpStart++;
            builder.append(longToIp(numOfIpStart));
            builder.append("\n");
        }
        return builder.toString();
    }
    protected static long ipToLong(String stringIp) throws UnknownHostException {
        InetAddress address = InetAddress.getByName(stringIp);
        ByteBuffer byteBuffer = ByteBuffer.allocate(4).wrap(address.getAddress());
        return (long)byteBuffer.getInt();
    }
    private static String longToIp(long longIp) throws UnknownHostException {
        ByteBuffer byteBuffer = ByteBuffer.allocate(4).putInt((int)longIp);
        InetAddress address = InetAddress.getByAddress(byteBuffer.array());
        return address.getHostAddress();
    }
    }

